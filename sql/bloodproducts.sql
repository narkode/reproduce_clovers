SELECT admissionid, start, stop, item, dose, administered, fluidin
FROM drugitems
WHERE ordercategoryid = 61 -- Infuus - Bloedproducten
AND administered > 0
AND start < %(fluid_period)s 
AND admissionid IN %(sepsis_ids)s

# Reproduce CLOVERS

## Introduction
The notebook and code in this repository can be used to determine the 90-day
mortality of septic IMC/ICU patients of the
[AmsterdamUMCdb](https://github.com/AmsterdamUMC/AmsterdamUMCdb) depending on
their fluid intake of the first 24 hours after admission. The aim is to
reproduce the core question of the
[CLOVERS-trial](https://www.nejm.org/doi/full/10.1056/NEJMoa2212663).

## Prerequisites and HOWTO
- A local installation of the AmsterdamUMCdb
- Rename config.SAMPLE.ini to config.ini and adapt it to your local settings

## CAVE
The results are currently not usable, as the Amsterdamumcdb has no reliable data on 90-day mortality. The problem will hopefully be solved in a future database version.
